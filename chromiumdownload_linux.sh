if [ -f ./chrome-linux.zip ]; then
  echo "WARNING: Previous script run did not clean up"
  rm ./chrome-linux.zip
fi

if [ ! -f prev ]; then
  echo "No previous build logged"
  echo "-1" >prev
fi

if [[ `wget -q http://commondatastorage.googleapis.com/chromium-browser-snapshots/Linux/LAST_CHANGE -O-` == `cat prev` ]]; then
  echo "No build change (`wget -q http://commondatastorage.googleapis.com/chromium-browser-snapshots/Linux/LAST_CHANGE -O-` = `cat prev`)"
  exit 1
else
  echo "New build (previous: `cat prev`, new: `wget -q http://commondatastorage.googleapis.com/chromium-browser-snapshots/Linux/LAST_CHANGE -O-`)"
fi
echo Downloading http://commondatastorage.googleapis.com/chromium-browser-snapshots/`wget -q http://commondatastorage.googleapis.com/chromium-browser-snapshots/Linux/LAST_CHANGE -O-`/chrome-linux.zip
wget http://commondatastorage.googleapis.com/chromium-browser-snapshots/Linux/`wget -q http://commondatastorage.googleapis.com/chromium-browser-snapshots/Linux/LAST_CHANGE -O-`/chrome-linux.zip

if [[ `ps -A | grep chrome | wc -l` != 0 ]]; then
  echo "`ps -A | grep chrome | wc -l` Chrome Processes Running. Attempting to Kill"
  for a in `ps -A | grep chrome | awk '{ print $1}'`
    do
      echo Killing PID $a
      kill -9 $a
    done
else
  echo "No Chrome Processes Running"
fi

echo Extracting zip
unzip chrome-linux.zip
echo Done

wget -q http://commondatastorage.googleapis.com/chromium-browser-snapshots/Linux/LAST_CHANGE -O prev
echo "New build recorded (`cat prev`)"
rm ./chrome-linux.zip

echo Starting Chromium...
cd chrome-linux
chrome &
cd ..
