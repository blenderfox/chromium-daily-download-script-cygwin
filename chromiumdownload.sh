if [ -f ./mini_installer.exe ]; then
  echo "WARNING: Previous script run did not clean up"
  rm ./mini_installer.exe
fi

if [ ! -f prev ]; then
  echo "No previous build logged"
  echo "-1" >prev
fi

if [[ `wget -q http://commondatastorage.googleapis.com/chromium-browser-snapshots/Win/LAST_CHANGE -O-` == `cat prev` ]]; then
  echo "No build change (`wget -q http://commondatastorage.googleapis.com/chromium-browser-snapshots/Win/LAST_CHANGE -O-` = `cat prev`)"
  exit 1
else
  echo "New build (previous: `cat prev`, new: `wget -q http://commondatastorage.googleapis.com/chromium-browser-snapshots/Win/LAST_CHANGE -O-`)"
fi
echo Downloading http://commondatastorage.googleapis.com/chromium-browser-snapshots/`wget -q http://commondatastorage.googleapis.com/chromium-browser-snapshots/Win/LAST_CHANGE -O-`/mini_installer.exe
wget http://commondatastorage.googleapis.com/chromium-browser-snapshots/Win/`wget -q http://commondatastorage.googleapis.com/chromium-browser-snapshots/Win/LAST_CHANGE -O-`/mini_installer.exe

if [[ `ps -W | grep chrome | wc -l` != 0 ]]; then
  echo "`ps -W | grep chrome | wc -l` Chrome Processes Running. Attempting to Kill"
  for a in `ps -W | grep chrome | awk '{ print $1}'`
    do
      echo Killing PID $a
      /bin/kill -f $a #This is the cygwin kill, not the bash kill
    done
else
  echo "No Chrome Processes Running"
fi

echo "Running installer"
chmod +x ./mini_installer.exe
./mini_installer.exe
echo Done
wget -q http://commondatastorage.googleapis.com/chromium-browser-snapshots/Win/LAST_CHANGE -O prev
echo "New build recorded (`cat prev`)"
rm ./mini_installer.exe
#Commenting this out because even though Chrome starts in a separate process, closing the cygwin window
#still leaves a bash process running
#echo "Starting Chrome..."
#cd "$USERPROFILE\\Local Settings\\Application Data\\Chromium\\Application" 
#./chrome.exe &
#cd ~/